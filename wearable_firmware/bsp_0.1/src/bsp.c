/*****************************************************************************
* Product: "Blinky" example, EK-TM4C123GXL board, preemptive QK kernel
* Last updated for version 5.6.0
* Last updated on  2015-12-18
*
*                    Q u a n t u m     L e a P s
*                    ---------------------------
*                    innovating embedded systems
*
* Copyright (C) Quantum Leaps, LLC. All rights reserved.
*
* This program is open source software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Alternatively, this program may be distributed and modified under the
* terms of Quantum Leaps commercial licenses, which expressly supersede
* the GNU General Public License and are specifically designed for
* licensees interested in retaining the proprietary status of their code.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* Contact information:
* https://state-machine.com
* mailto:info@state-machine.com
*****************************************************************************/
#include "qpc.h"
#include "blinky.h"
#include "blinky_grn.h"
#include "bsp.h"
#include "rom.h"                 /* the built-in ROM functions (TI) */
#include "sysctl.h"              /* system control driver (TI) */
#include "gpio.h"                /* GPIO driver (TI) */
#include "adc_interface.h"
#include <stdio.h>
#include <stdlib.h>
#include "i2c_interface.h"
/* add other drivers if necessary... */

Q_DEFINE_THIS_FILE

typedef struct circ_buff
{
uint32_t head;
uint32_t tail;
struct circ_buff *next;
};


void ADC1Seq3_IRQHandler()
{
	QXK_ISR_ENTRY();
    uint32_t adc_data_out = (Read_reg(ADC_1_SSFIFO3));
    Write_reg(ADC_1__ICS,(1<<3));/*Clearing Interupt status*/
	QS_BEGIN(BLINKY_RED,AO_Blinky) /* application-specific record begin */
	QS_STR("Adc Data Out \n");
	QS_U32(BLINKY_RED,adc_data_out);
	QS_END()
	QXK_ISR_EXIT();

}

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
* DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
*/
enum KernelUnawareISRs { /* see NOTE00 */
    /* ... */
	UART0_PRIO,
    MAX_KERNEL_UNAWARE_CMSIS_PRI  /* keep always last */
};
/* "kernel-unaware" interrupts can't overlap "kernel-aware" interrupts */
Q_ASSERT_COMPILE(MAX_KERNEL_UNAWARE_CMSIS_PRI <= QF_AWARE_ISR_CMSIS_PRI);

enum KernelAwareISRs {
    SYSTICK_PRIO = QF_AWARE_ISR_CMSIS_PRI, /* see NOTE00 */
    /* ... */
	GPIOA_PRIO,
    MAX_KERNEL_AWARE_CMSIS_PRI /* keep always last */
};
/* "kernel-aware" interrupts should not overlap the PendSV priority */
Q_ASSERT_COMPILE(MAX_KERNEL_AWARE_CMSIS_PRI <= (0xFF >>(8-__NVIC_PRIO_BITS)));

/* ISRs defined in this BSP ------------------------------------------------*/
void SysTick_Handler(void);
//void GPIOPortA_IRQHandler(void);


void GPIOPortA_IRQHandler(void){
	QXK_ISR_ENTRY();
	QS_BEGIN(BLINKY_RED,AO_Blinky)
	QS_STR("GPIOA Interrpt");
	//QS_U32(BLINKY_RED,GPIOA->RIS);
	QS_END()

	if((GPIOA->RIS & (1U << 3)) !=0)
	{
	QS_BEGIN(BLINKY_RED,AO_Blinky)
	QS_STR("GPIOA Interrpt");
	//QS_U32(BLINKY_RED,GPIOA->RIS);
	QS_END()
	}
	GPIOA->ICR = 0xFFU;

	QXK_ISR_EXIT();
}

void GPIOPortF_IRQHandler(void){
	QXK_ISR_ENTRY();
	//Q_ASSERT(0);


	if((GPIOF->RIS & (1U << 4)) !=0)
	{
	QS_BEGIN(BLINKY_RED,AO_Blinky)
	QS_STR("GPIOF Interrpt");
	//QS_U32(BLINKY_RED,GPIOA->RIS);
	QS_END()
	}
	GPIOF->ICR = 0xFFU;
	QXK_ISR_EXIT();
}


/* Local-scope objects -----------------------------------------------------*/
#define LED_RED     (1U << 1)
#define LED_GREEN   (1U << 3)
#define LED_BLUE    (1U << 2)
#define GPIO_ENA    (1U << 3)
#define BTN_SW1     (1U << 4)
#define BTN_SW2     (1U << 0)

/* ISRs used in this project ===============================================*/
void SysTick_Handler(void) {
    QXK_ISR_ENTRY();   /* inform QK about entering an ISR */
    QF_TICK_X(0U, (void *)0); /* process time events for rate 0 */
    QXK_ISR_EXIT();  /* inform QK about exiting an ISR */
}

#ifdef Q_SPY
/*
* ISR for receiving bytes from the QSPY Back-End
* NOTE: This ISR is "QF-unaware" meaning that it does not interact with
* the QF/QXK and is not disabled. Such ISRs don't need to call QXK_ISR_ENTRY/
* QXK_ISR_EXIT and they cannot post or publish events.
*/
void UART0_IRQHandler(void) {
    uint32_t status = UART0->RIS; /* get the raw interrupt status */
    UART0->ICR = status;          /* clear the asserted interrupts */

    while ((UART0->FR & UART_FR_RXFE) == 0) { /* while RX FIFO NOT empty */
        uint32_t b = UART0->DR;
        QS_RX_PUT(b);
    }
}
#else
void UART0_IRQHandler(void) {}
#endif

/* BSP functions ===========================================================*/
void BSP_init(void) {
    /* NOTE: SystemInit() already called from the startup code
    *  but SystemCoreClock needs to be updated
    */
    SystemCoreClockUpdate();

    /* configure the FPU usage by choosing one of the options... */
#if 1
    /* OPTION 1:
    * Use the automatic FPU state preservation and the FPU lazy stacking.
    *
    * NOTE:
    * Use the following setting when FPU is used in more than one task or
    * in any ISRs. This setting is the safest and recommended, but requires
    * extra stack space and CPU cycles.
    */
    FPU->FPCCR |= (1U << FPU_FPCCR_ASPEN_Pos) | (1U << FPU_FPCCR_LSPEN_Pos);
#else
    /* OPTION 2:
    * Do NOT to use the automatic FPU state preservation and
    * do NOT to use the FPU lazy stacking.
    *
    * NOTE:
    * Use the following setting when FPU is used in ONE task only and not
    * in any ISR. This setting is very efficient, but if more than one task
    * (or ISR) start using the FPU, this can lead to corruption of the
    * FPU registers. This option should be used with CAUTION.
    */
    FPU->FPCCR &= ~((1U << FPU_FPCCR_ASPEN_Pos)
                   | (1U << FPU_FPCCR_LSPEN_Pos));
#endif

    /* enable clock for to the peripherals used by this application... */
    SYSCTL->RCGCGPIO |= (1U << 5) |(1<<3) |1U ; /* enable Run mode for GPIOF */

    /* configure the LEDs and push buttons */
    GPIOF->DIR |= (LED_RED | LED_GREEN | LED_BLUE | (1U <<4));/* set direction: output */
    GPIOF->DEN |= (LED_RED | LED_GREEN | LED_BLUE | (1U <<4)); /* digital enable */
    GPIOF->DATA_Bits[LED_RED]   = 0U; /* turn the LED off */
    GPIOF->DATA_Bits[LED_GREEN] = 0U; /* turn the LED off */
    GPIOF->DATA_Bits[LED_BLUE]  = 0U; /* turn the LED off */
    GPIOF->DATA_Bits[(1U <<4)]  = 0U; /* turn the LED off */

    GPIOF->DIR &= ~(BTN_SW1);
    GPIOF->DEN |= (BTN_SW1);
    GPIOF->PUR |= (BTN_SW1);
    GPIOF->IS  &= ~(BTN_SW1);
    GPIOF->IBE &= ~(BTN_SW1);
    GPIOF->IEV &= ~(BTN_SW1);
    GPIOF->IM  |= (BTN_SW1);

    GPIOA->DIR &= ~(GPIO_ENA);
    GPIOA->DEN |= (GPIO_ENA);
    GPIOA->PUR |= (GPIO_ENA);
    GPIOA->IS  &= ~(GPIO_ENA);
    GPIOA->IBE |= (GPIO_ENA);
    //GPIOA->IEV &= (GPIO_ENA);
    GPIOA->IM  |= (GPIO_ENA);

   // GPIOA->ADCCTL |= (GPIO_ENA);
    adc_init(1);
    config_sample_seq();
    i2c_port_config(1);
    /* configure the Buttons */
    GPIOF->DIR &= ~(BTN_SW1 | BTN_SW2); /*  set direction: input */
    ROM_GPIOPadConfigSet(GPIOF_BASE, (BTN_SW1 | BTN_SW2),
                         GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

    //gpio_conf();
    if (QS_INIT((void *)0) == 0U) {
          Q_ERROR();
      }

      QS_USR_DICTIONARY(BLINKY_STAT);
      QS_USR_DICTIONARY(BLINKY_RED);
      QS_USR_DICTIONARY(BLINKY_GRN);
}
uint32_t rev_id =0;
/*..........................................................................*/
void BSP_ledOff(void) {
    QS_BEGIN(BLINKY_RED,AO_Blinky) /* application-specific record begin */
    QS_STR("Red Led Off");
    QS_STR("GPIO Input Read Off");
    QS_U32(BLINKY_RED,GPIOA->DATA_Bits[GPIO_ENA]);
    QS_STR("I2C VerID Read");

    //QS_U32(BLINKY_RED,read_Data_i2c(0xFE,START));
    QS_END()

    GPIOF->DATA_Bits[LED_RED] = 0x00U;
    GPIOF->DATA_Bits[LED_RED] = 0U;
    GPIOF->DATA_Bits[(1U <<4)]  = 0xFFU;
    GPIOF->DATA_Bits[(1U <<4)]  = 0U <<3;




}
/*..........................................................................*/
void BSP_ledOn(void) {
    /* exercise the FPU with some floating point computations */
    float volatile x = 3.1415926F;
    x = x + 2.7182818F;

    QS_BEGIN(BLINKY_RED,AO_Blinky) /* application-specific record begin */
    QS_STR("Red Led On");
    QS_END()

    GPIOF->DATA_Bits[LED_RED] = 0x00U;
    GPIOF->DATA_Bits[(1U <<4)]  = 0xFFU <<3 ;

}
/**/
void gpio_conf(void) {


}

void BSP_ledOn_grn(void) {
    float volatile x = 3.1415926F;
    x = x + 2.7182818F;
    QS_BEGIN(BLINKY_GRN,AO_Blinky_grn) /* application-specific record begin */
    QS_STR("Grn Led On");
    QS_END()

    GPIOF->DATA_Bits[LED_GREEN] = 0xFFU;

}

void BSP_ledOff_grn(void) {
    QS_BEGIN(BLINKY_GRN,AO_Blinky_grn) /* application-specific record begin */
    QS_STR("Grn Led Off");
    QS_END()
	GPIOF->DATA_Bits[LED_GREEN] = 0U;

}
/* QF callbacks ============================================================*/
void QF_onStartup(void) {
    /* set up the SysTick timer to fire at BSP_TICKS_PER_SEC rate */
    SysTick_Config(SystemCoreClock / BSP_TICKS_PER_SEC);

    /* assing all priority bits for preemption-prio. and none to sub-prio. */
    NVIC_SetPriorityGrouping(0U);

    /* set priorities of ALL ISRs used in the system, see NOTE00
    *
    * !!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    * Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
    * DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
    */
    NVIC_SetPriority(UART0_IRQn,UART0_PRIO);
    NVIC_SetPriority(SysTick_IRQn,SYSTICK_PRIO);
    //NVIC_SetPriority(GPIOA_IRQn,GPIOA_PRIO);
    NVIC_SetPriority(ADC1SS3_IRQn,QF_AWARE_ISR_CMSIS_PRI+2);
    NVIC_SetPriority(GPIOF_IRQn,QF_AWARE_ISR_CMSIS_PRI+1);
    NVIC_SetPriority(GPIOA_IRQn,QF_AWARE_ISR_CMSIS_PRI+1);
        /* ... */

        /* enable IRQs... */
    NVIC_EnableIRQ(GPIOA_IRQn);
    NVIC_EnableIRQ(GPIOF_IRQn);
    NVIC_EnableIRQ(ADC1SS3_IRQn);

    #ifdef Q_SPY
        NVIC_EnableIRQ(UART0_IRQn);  /* UART0 interrupt used for QS-RX */
    #endif
    }
    /* ... */

    /* enable IRQs... */
//}
/*..........................................................................*/
void QF_onCleanup(void) {
}
/*..........................................................................*/
void QXK_onIdle(void) {
    /* toggle LED2 on and then off, see NOTE01 */
    QF_INT_DISABLE();
    GPIOF->DATA_Bits[LED_BLUE] ^= 0xFFU;
    QF_INT_ENABLE();

#ifdef Q_SPY
    QS_rxParse();  /* parse all the received bytes */

    if ((UART0->FR & UART_FR_TXFE) != 0U) {  /* TX done? */
        uint16_t fifo = UART_TXFIFO_DEPTH;   /* max bytes we can accept */
        uint8_t const *block;

        QF_INT_DISABLE();
        block = QS_getBlock(&fifo);  /* try to get next block to transmit */
        QF_INT_ENABLE();

        while (fifo-- != 0) {  /* any bytes in the block? */
            UART0->DR = *block++;  /* put into the FIFO */
        }
    }
#elif defined NDEBUG
    /* Put the CPU and peripherals to the low-power mode.
    * you might need to customize the clock management for your application,
    * see the datasheet for your particular Cortex-M3 MCU.
    */
    __WFI(); /* Wait-For-Interrupt */
#endif
}

void Write_reg(int addr,int val)
{
	*((unsigned int *)addr) |= val;
}

uint32_t Read_reg(uint32_t addr)
{
return *((unsigned int *)addr);
}

void Clear_Reg_Bit(int addr,int val)
{
    *((unsigned int *)addr) &= ~(1<<val);
}

/*..........................................................................*/
void Q_onAssert(char const *module, int loc) {
    /*
    * NOTE: add here your application-specific error handling
    */
    (void)module;
    (void)loc;
    QS_ASSERTION(module, loc, (uint32_t)10000U); /* report assertion to QS */
    NVIC_SystemReset();
    while(1)
    {

    }
}

/* QS callbacks ============================================================*/
#ifdef Q_SPY
/*..........................................................................*/
uint8_t QS_onStartup(void const *arg) {
    static uint8_t qsTxBuf[2*1024]; /* buffer for QS transmit channel */
    static uint8_t qsRxBuf[100];    /* buffer for QS receive channel */
    uint32_t tmp;

    QS_initBuf  (qsTxBuf, sizeof(qsTxBuf));
    QS_rxInitBuf(qsRxBuf, sizeof(qsRxBuf));

    /* enable clock for UART0 and GPIOA (used by UART0 pins) */
    SYSCTL->RCGCUART |= (1U << 0); /* enable Run mode for UART0 */
    SYSCTL->RCGCGPIO |= (1U << 0); /* enable Run mode for GPIOA */

    /* configure UART0 pins for UART operation */
    tmp = (1U << 0) | (1U << 1);
    GPIOA->DIR   &= ~tmp;
    GPIOA->SLR   &= ~tmp;
    GPIOA->ODR   &= ~tmp;
    GPIOA->PUR   &= ~tmp;
    GPIOA->PDR   &= ~tmp;
    GPIOA->AMSEL &= ~tmp;  /* disable analog function on the pins */
    GPIOA->AFSEL |= tmp;   /* enable ALT function on the pins */
    GPIOA->DEN   |= tmp;   /* enable digital I/O on the pins */
    GPIOA->PCTL  &= ~0x00U;
    GPIOA->PCTL  |= 0x11U;

    /* configure the UART for the desired baud rate, 8-N-1 operation */
    tmp = (((SystemCoreClock * 8U) / UART_BAUD_RATE) + 1U) / 2U;
    UART0->IBRD   = tmp / 64U;
    UART0->FBRD   = tmp % 64U;
    UART0->LCRH   = (0x3U << 5); /* configure 8-N-1 operation */
    UART0->LCRH  |= (0x1U << 4); /* enable FIFOs */
    UART0->CTL    = (1U << 0)    /* UART enable */
                    | (1U << 8)  /* UART TX enable */
                    | (1U << 9); /* UART RX enable */

    /* configure UART interrupts (for the RX channel) */
    UART0->IM   |= (1U << 4) | (1U << 6); /* enable RX and RX-TO interrupt */
    UART0->IFLS |= (0x2U << 2);    /* interrupt on RX FIFO half-full */
    /* NOTE: do not enable the UART0 interrupt yet. Wait till QF_onStartup() */

    QS_tickPeriod_ = SystemCoreClock / BSP_TICKS_PER_SEC;
    QS_tickTime_ = QS_tickPeriod_; /* to start the timestamp at zero */

    /* setup the QS filters... */
    //QS_FILTER_ON(QS_SM_RECORDS); /* state machine records */
    //QS_FILTER_ON(QS_UA_RECORDS); /* all usedr records */
    //QS_FILTER_ON(QS_MUTEX_LOCK);
    //QS_FILTER_ON(QS_MUTEX_UNLOCK);
    QS_FILTER_ON(BLINKY_RED);
    QS_FILTER_ON(BLINKY_GRN);
    QS_FILTER_ON(PERIPHERAL_WRITE);
    return (uint8_t)1; /* return success */
}
/*..........................................................................*/
void QS_onCleanup(void) {
}
/*..........................................................................*/
QSTimeCtr QS_onGetTime(void) {  /* NOTE: invoked with interrupts DISABLED */
    if ((SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) == 0) { /* not set? */
        return QS_tickTime_ - (QSTimeCtr)SysTick->VAL;
    }
    else { /* the rollover occured, but the SysTick_ISR did not run yet */
        return QS_tickTime_ + QS_tickPeriod_ - (QSTimeCtr)SysTick->VAL;
    }
}
/*..........................................................................*/
void QS_onFlush(void) {
    uint16_t fifo = UART_TXFIFO_DEPTH; /* Tx FIFO depth */
    uint8_t const *block;
    while ((block = QS_getBlock(&fifo)) != (uint8_t *)0) {
        /* busy-wait as long as TX FIFO has data to transmit */
        while ((UART0->FR & UART_FR_TXFE) == 0) {
        }

        while (fifo-- != 0U) {    /* any bytes in the block? */
            UART0->DR = *block++; /* put into the TX FIFO */
        }
        fifo = UART_TXFIFO_DEPTH; /* re-load the Tx FIFO depth */
    }
}
/*..........................................................................*/
/*! callback function to reset the target (to be implemented in the BSP) */
void QS_onReset(void) {
    NVIC_SystemReset();
}
/*..........................................................................*/
/*! callback function to execute a user command (to be implemented in BSP) */
void QS_onCommand(uint8_t cmdId,
                  uint32_t param1, uint32_t param2, uint32_t param3)
{
    void assert_failed(char const *module, int loc);
    (void)cmdId;
    (void)param1;
    (void)param2;
    (void)param3;
    QS_BEGIN(COMMAND_STAT, (void *)1) /* application-specific record begin */
        QS_U8(2, cmdId);
        QS_U32(8, param1);
        QS_U32(8, param2);
        QS_U32(8, param3);
    QS_END()

    if (cmdId == 10U) {
        Q_ERROR();
    }
    else if (cmdId == 11U) {
        assert_failed("QS_onCommand", 123);
    }
}

#endif /* Q_SPY */
