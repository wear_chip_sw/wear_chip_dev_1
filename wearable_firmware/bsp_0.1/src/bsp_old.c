/* Board Support Package */
#include "TM4C123GH6PM.h"
#include "bsp.h"

__attribute__((naked)) void assert_failed (char const *file, int line) {
    /* TBD: damage control */
    NVIC_SystemReset(); /* reset the system */
}

void SysTick_Handler(void) {
    GPIOF_AHB->DATA_Bits[LED_BLUE] ^= LED_BLUE;
}
/*Function To Write ARM Registers
 * @Parameters
 * addr=Register Address to Write,
 * val=Value to Write on Register
 * */
void Write_reg(int addr,int val)
{


    *((unsigned int *)addr) |= val;
}

uint32_t Read_reg(uint32_t addr)
{
return *((unsigned int *)addr);
}

void Clear_Reg_Bit(int addr,int val)
{
    *((unsigned int *)addr) &= ~(1<<val);
}

uint32_t get_os_timestamp()
{
return SysTick->VAL ;
}
