/*
 * ARM_ADDERS.h
 *
 *  Created on: 10-Jun-2018
 *      Author: MANMATH
 */

#ifndef BSP_0_1_ARM_ADDERS_H_
#define BSP_0_1_ARM_ADDERS_H_
/*SPI Memory Address */
#define RCGCSSI_PORT                0x400FE61C
#define RCGCGPIO_CLK                0x400FE608
#define GPIO_CTL_SPI                0x4000552C
#define GPIO_AFSL_SPI               0x40005420
#define GPIO_DIG_ENB                0x4000551C
#define SSI2_SSCR1                  0x4000A004
#define SSI2_SSICC                  0x4000AFC8
#define SSI2_SSICPSR                0x4000A010
#define SSI2_SSICR0                 0x4000A000
#define SSI2_SSIDR                  0x4000A008
#define SSI2_SSISR                  0x4000A00C
#define SSI2_SSIDMACTL              0x4000A024
#define SSI2_DMA_TX_ENB             0x2
#define SSI2_DMA_RX_ENB             0x1
/*UART Memory Address */
/*Register for Enable and Disable Clock gating for UART ports*/
#define UART_CLK__GATE 0x400FE618
#define GPIO_CLK_GATE 0x400FE608
#define GPIO_AFSEL 0x40004420
#define PMCN_REG 0x4000452C
#define GPIO_DATA_ENB 0x4000451C
#define UART0_CTL 0x4000C030
#define UART0_IBDR 0x4000C024
#define UART0_FBDR 0x4000C028
#define UART0_LCRH 0x4000C02C
#define UART0_CC 0x4000CFC8
#define UART0_FR 0x4000C018
#define UART0_DR 0x4000C000

/*                 UDMA Registres                   */

#define UDMA_CLK__GATE              0x400FE60C
#define UDMA_CLK__GATE_ENABLE               1
#define UDMA_CLK__GATE_DISABLE              0
#define UDMA_CONFIG                 0x400FF004
#define UDMA_MASTER_ENB                     1
#define UDMA_MASTER_DIS                     0
#define DMA_CTL_BASE                0x400FF008
#define DMA_CTL_BASE__CLEAR_DATA    0x00000000
#define DMA_CHANEL_MAP_01           0x400FF514
#define DMA_CHA_ASGN_SSI2_TX        0x00000002
#define DMA_CHA_PRIO_SET            0x400FF038
#define DMA_CHA_PRIO_SSI2__TX               13
#define DMA_CHA_ALT_CLEAR           0x400FF034
#define DMA_CHA_ALT_CLR_SSI2_TX             13
#define DMA_CHA_USR_BST_CLR         0x400FF01C
#define DMA_CHA_USR_BST_SSI2_TX             13
#define DMA_CHA_USRMSK_CLR          0x400FF024
#define DMA_CHA_USRMSK_CLR_SSI2_TX          13
#define DMA_CHA_SET_ENABLE          0x400FF028
#define DMA_CHA_ENB_SSI2_TX                 13
#define DMA_CHAIS                   0x400FF504
#define DMA_CHAIS_CHAN__NUM                 13
#define DMA_CHANNEL_ENA_SET         0x400FF028
#define DMA_CHANNEL_SW__REQ         0x400FF014
#define DMA_USE_BURST_CLEAR         0x400FF018


/*                  GPIO Registers                      */

#define GPIO_PORT_A_APB             0x40004000
#define GPIO_PORT_B_APB             0x40005000
#define GPIO_PORT_C_APB             0x40006000
#define GPIO_PORT_D_APB             0x40007000
#define GPIO_PORT_E_APB             0x40024000
#define GPIO_PORT_F_APB             0x40025000
//off-set value

/*                      I2C Registers              */

#define RCGCI2C_PORT                0x400FE620
#define I2C_CONFIG                  0x40021020
#define I2C___MTPR                  0x4002100C
#define I2C_SLAVE_ADDR              0x40021000
#define I2C_DATA_REG                0X40021008
#define I2C_MCS_REG                 0x40021004

/*                      ADC Registers               */
#define RCGCADC__PORT               0x400FE638
#define GPIOADC__DIR                0x40024400
#define ADC_1__ACTSS                0x40039000
#define ADC_1___EMUX                0x40039014
#define ADC_1_SSMUX3                0x400390A0
#define ADC_1_SSCTL3                0x400390A4
#define ADC_1_INTMSK                0x40039008
#define ADC_1__ICS                  0x4003900C
#define ADC_1_SSFIFO3               0x400390A8

#endif /* BSP_0_1_ARM_ADDERS_H_ */
