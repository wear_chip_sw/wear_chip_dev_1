/*****************************************************************************
* Product: DPP example
* Last Updated for Version: 5.6.0
* Date of the Last Update:  2015-11-22
*
*                    Q u a n t u m     L e a P s
*                    ---------------------------
*                    innovating embedded systems
*
* Copyright (C) Quantum Leaps, LLC. All rights reserved.
*
* This program is open source software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Alternatively, this program may be distributed and modified under the
* terms of Quantum Leaps commercial licenses, which expressly supersede
* the GNU General Public License and are specifically designed for
* licensees interested in retaining the proprietary status of their code.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* Contact information:
* https://state-machine.com
* mailto:info@state-machine.com
*****************************************************************************/
#ifndef bsp_h
#define bsp_h
#include "qpc.h"
#include "ARM_ADDERS.h"
#include "TM4C123GH6PM.h"        /* the device specific header (TI) */
#define BSP_TICKS_PER_SEC    100U

void BSP_init(void);
void BSP_displayPaused(uint8_t paused);
void BSP_displayPhilStat(uint8_t n, char_t const *stat);
void BSP_terminate(int16_t result);

void BSP_randomSeed(uint32_t seed);   /* random seed */
uint32_t BSP_random(void);            /* pseudo-random generator */

/* for testing... */
void BSP_wait4SW1(void);
void BSP_ledOn(void);
void BSP_ledOff(void);

void BSP_ledOn_grn(void);
void BSP_ledOff_grn(void);
void gpio_conf(void);

typedef enum{
FAILURE_INVALID_PARAM = -2,
FAILURE,
SUCCESS
}result;

#ifdef Q_SPY

    QSTimeCtr QS_tickTime_;
    QSTimeCtr QS_tickPeriod_;

    /* QS identifiers for non-QP sources of events */

    #define UART_BAUD_RATE      115200U
    #define UART_FR_TXFE        (1U << 7)
    #define UART_FR_RXFE        (1U << 4)
    #define UART_TXFIFO_DEPTH   16U

    enum AppRecords { /* application-specific trace records */
      BLINKY_STAT = QS_USER,
		BLINKY_RED,
		BLINKY_GRN,
		PERIPHERAL_READ,
		PERIPHERAL_WRITE,
		COMMAND_STAT
    };

#endif

/*GPIO Port Base Address Mapings*/
#define config_pa  ((gpio_config *)(GPIO_PORT_A_APB+0x400))
#define config_pb  ((gpio_config *)(GPIO_PORT_B_APB+0x400))
#define config_pc  ((gpio_config *)(GPIO_PORT_C_APB+0x400))
#define config_pd  ((gpio_config *)(GPIO_PORT_D_APB+0x400))
#define config_pe  ((gpio_config *)(GPIO_PORT_E_APB+0x400))
#define config_pf  ((gpio_config *)(GPIO_PORT_F_APB+0x400))

typedef enum{
    gpio_p0,
    gpio_p1,
    gpio_p2,
    gpio_p3,
    gpio_p4,
    gpio_p5,
    gpio_p6,
    gpio_p7,

}gpio_grp_p;

/*Function for Write a Register */

void Write_reg(int addr,int val);
uint32_t Read_reg(uint32_t addr);
void Clear_Reg_Bit(int addr,int val);

#endif /* bsp_h */
