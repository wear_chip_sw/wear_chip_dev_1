/*****************************************************************************
* Product: Simple Blinky example
* Last Updated for Version: 5.6.5
* Date of the Last Update:  2016-06-03
*
*                    Q u a n t u m     L e a P s
*                    ---------------------------
*                    innovating embedded systems
*
* Copyright (C) Quantum Leaps, LLC. All rights reserved.
*
* This program is open source software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published
* by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Alternatively, this program may be distributed and modified under the
* terms of Quantum Leaps commercial licenses, which expressly supersede
* the GNU General Public License and are specifically designed for
* licensees interested in retaining the proprietary status of their code.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* Contact information:
* Web:   www.state-machine.com
* Email: info@state-machine.com
*****************************************************************************/
#include "qpc.h"
#include "blinky_grn.h"
#include "blinky.h"
#include "bsp.h"

Q_DEFINE_THIS_FILE

/*..........................................................................*/
typedef struct {     /* the Blinky active object */
    QActive super;   /* inherit QActive */

    QTimeEvt timeEvt; /* private time event generator */
} Blinky_grn;

static Blinky_grn l_blinky_grn; /* the Blinky active object */

QActive * const AO_Blinky_grn = &l_blinky_grn.super;

/* hierarchical state machine ... */
static QState Blinky_grn_initial(Blinky_grn * const me, QEvt const * const e);
static QState Blinky_grn_off    (Blinky_grn * const me, QEvt const * const e);
static QState Blinky_grn_on     (Blinky_grn * const me, QEvt const * const e);

/*..........................................................................*/
void Blinky_grn_ctor(void) {
    Blinky_grn * const me = &l_blinky_grn;
    QActive_ctor(&me->super, Q_STATE_CAST(&Blinky_grn_initial));
    QTimeEvt_ctorX(&me->timeEvt, &me->super, TIMEOUT_GRN_SIG, 0U);
}

/* HSM definition ----------------------------------------------------------*/
QState Blinky_grn_initial(Blinky_grn * const me, QEvt const * const e) {
    (void)e; /* avoid compiler warning about unused parameter */

    /* arm the time event to expire in half a second and every half second */
    //QTimeEvt_armX(&me->timeEvt, BSP_TICKS_PER_SEC*2U, BSP_TICKS_PER_SEC*2U);
    QTimeEvt_armX(&me->timeEvt,BSP_TICKS_PER_SEC*4U,BSP_TICKS_PER_SEC*4U);
    return Q_TRAN(&Blinky_grn_on);
}
/*..........................................................................*/
QState Blinky_grn_off(Blinky_grn * const me, QEvt const * const e) {
    QState status;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
            BSP_ledOff_grn();
            status = Q_HANDLED();
            break;
        }
        case TIMEOUT_GRN_SIG: {
            status = Q_TRAN(&Blinky_grn_on);
            break;
        }
        default: {
            status = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status;
}
/*..........................................................................*/
QState Blinky_grn_on(Blinky_grn * const me, QEvt const * const e) {
    QState status;
    switch (e->sig) {
        case Q_ENTRY_SIG: {
            BSP_ledOn_grn();

            status = Q_HANDLED();
            break;
        }
        case TIMEOUT_GRN_SIG: {
            status = Q_TRAN(&Blinky_grn_off);
            break;
        }

        case LED_OFF_SIG: {
                    status = Q_TRAN(&Blinky_grn_off);
                    break;
                }
        default: {
            status = Q_SUPER(&QHsm_top);
            break;
        }
    }
    return status;
}




