#include "Driver_unit_test.h"
#include "spi_interface.h"
#include "gpio_driver.h"
#include "bsp.h"
//TODO: Replace this file with QM tool generated file.

static Driver_unit_test driver_unit_test_t; /* Driver_unit_test active object */
QActive * const AO_Driver_unit_test = &driver_unit_test_t.super;
/* hierarchical state machine ... */
static QState Driver_unit_test_initial(Driver_unit_test * const me, QEvt const * const e);
static QState Driver_unit_test_xfer(Driver_unit_test * const me, QEvt const * const e);


void Driver_unit_test_ctor(void)
{
  Driver_unit_test * const me = &driver_unit_test_t;
  QActive_ctor(&me->super, Q_STATE_CAST(&Driver_unit_test_initial));
  QTimeEvt_ctorX(&me->timeEvt, &me->super, DRIVER_UNIT_TEST_SPI_WRITE_SIG, 0U);
  //QTimeEvt_ctorX(&me->timeEvt, &me->super, DRIVER_UNIT_TEST_SPI_WRITE_SIG, 0U);
}

static QState Driver_unit_test_initial(Driver_unit_test * const me, QEvt const * const e)
{
  QTimeEvt_armX(&me->timeEvt,50*8U,0);
  return Q_TRAN(&Driver_unit_test_xfer);
}
void Peripheral_Write()
{
  QS_BEGIN(PERIPHERAL_WRITE, AO_Driver_unit_test)  //application-specific record begin
  QS_STR("Peripheral Write Requested");
  QS_END()
  spi_gpio_configure configure_spi;
  configure_spi.port_num = 2;
  configure_spi.spio_clk = 58;
  configure_spi.spio_ss = 57;
  configure_spi.spio_tx = 4;
  configure_spi.spio_rx = 1;
  spi_port_config(2);
  spi_port_config_struct(configure_spi);

}

static QState Driver_unit_test_xfer(Driver_unit_test * const me, QEvt const * const e)
{
  QState status;
  switch (e->sig) {
      case Q_ENTRY_SIG: {
        status = Q_HANDLED();
        break;
      }

      case DRIVER_UNIT_TEST_SPI_WRITE_SIG: {
	Peripheral_Write();
	Async_write_spi(0x0A);
        status = Q_HANDLED();
        break;
      }

      default: {
          status = Q_SUPER(&QHsm_top);
          break;
      }
  }
  return status;
}





