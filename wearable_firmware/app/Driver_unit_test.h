/*
 * Driver_unit_test.h
 *
 *  Created on: 13-Feb-2021
 *      Author: MANMATH
 */

#ifndef _DRIVER_UNIT_TEST_H_
#define _DRIVER_UNIT_TEST_H_
#include "qpc.h"

enum driver_unit_test {
	DRIVER_UNIT_TEST_START = Q_USER_SIG,
	DRIVER_UNIT_TEST_SPI_WRITE_SIG,
	DRIVER_UNIT_TEST_SPI_READ_SIG,
	DRIVER_UNIT_TEST_I2C_WRITE_SIG,
	DRIVER_UNIT_TEST_I2c_READ_SIG
};

/*..........................................................................*/
typedef struct {     /* the Blinky active object */
    QActive super;   /* inherit QActive */
    QTimeEvt timeEvt;
} Driver_unit_test;

void Driver_unit_test_ctor(void);

void Peripheral_Write();

void Peripheral_Read();
extern QActive * const AO_Driver_unit_test; /* opaque pointer */
#endif /* _DRIVER_UNIT_TEST_H_ */
