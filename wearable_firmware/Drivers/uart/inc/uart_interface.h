#include "TM4C123GH6PM.h"
#include "bsp.h"
#include <stdio.h>

typedef enum uart_num{
  UART_port0,
  UART_port1,
  UART_port2,
  UART_port3,
  UART_port4,
  UART_port5,
  UART_port6,
  UART_port7,
  UART_PORT_NA
};

/*Function to Write Register*/


/*Temporary Function for Clock gating
 * @param Reg Address
 *
 * */

void Init_uart_port(uint8_t uart_port);

void AsynWritedata(uint32_t data);
void Async_Uart_Write(uint8_t *data,uint32_t length);
uint32_t AsynReaddata();
