/*
 * uart_interface.c
 *
 *  Created on: 15-Apr-2018
 *      Author: MANMATH
 *  File Includes All Api and Low Level Driver to Support and Access UART Interface
 *
 */
#include "uart_interface.h"

void Init_uart_port(uint8_t uart_port)
{
if(uart_port >= UART_PORT_NA)
{    return ;}
Write_reg(UART_CLK__GATE,0x1<<uart_port);
Write_reg(GPIO_CLK_GATE,0x1);
Write_reg(GPIO_AFSEL,0x3);
Write_reg(PMCN_REG,(1<<0|1<<4));
Write_reg(GPIO_DATA_ENB,0x3);
Write_reg(UART0_CTL,0x0);
Write_reg(UART0_IBDR,104);
Write_reg(UART0_FBDR,11);
Write_reg(UART0_LCRH,0x3<<5|1<<4);
Write_reg(UART0_CC,0x0);
Write_reg(UART0_CTL,((1<<0)|(1<<8)|(1<<9)));
}
void AsynWritedata(uint32_t data)
{
while(((*((unsigned int *)UART0_FR))&(1<<5)) != 0);
  *((unsigned int *)UART0_DR)=data;

}

void Async_Uart_Write(uint8_t *data,uint32_t length)
{
for(int i=0;i<=length+1;i++)
{
    while(((*((unsigned int *)UART0_FR))&(1<<5)) != 0);
      *((unsigned int *)UART0_DR)=*data;
   data++;
}


}
uint32_t AsynReaddata()
{
    uint32_t data;
    if(((*((unsigned int *)UART0_FR))&(1<<6)) == 0)
    {
    while(((*((unsigned int *)UART0_FR))&(1<<4)) != 0)
        data=*((unsigned int *)UART0_DR);

    }
    return data;
}
