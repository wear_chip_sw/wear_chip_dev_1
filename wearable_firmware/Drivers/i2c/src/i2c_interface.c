/*
 * i2c_interface.c
 *
 *  Created on: 11-Aug-2018
 *      Author: MANMATH
 *
 *
 */
#include "i2c_interface.h"
#include <stdio.h>
#include <stdlib.h>
void Set_slave_addr(uint32_t addr, uint32_t bit_rw)
{
    Write_reg(I2C_SLAVE_ADDR,(addr<<1));
    Clear_Reg_Bit(I2C_SLAVE_ADDR,bit_rw);
}

void Write_Data(uint8_t data,uint8_t condition)
{
    Write_reg(I2C_DATA_REG,data);
    Write_reg(I2C_MCS_REG,((1<<condition)|(1<<0)|(1<<3)));
    while((Read_reg(I2C_MCS_REG) & 1) !=0);

}

int read_Data_i2c(uint8_t Addr,uint8_t condition)
{

    Data_tx_rx(false);//Mode changed to Receive
    while((Read_reg(I2C_MCS_REG) & 1) !=0);/*Wait for I2C bus to be free for reception*/
    Write_reg(I2C_DATA_REG,Addr);
    Write_reg(I2C_MCS_REG,((1<<condition)|(1<<0)|(1<<3)));
    int data=(Read_reg(I2C_DATA_REG));
    return data;

}
void Data_tx_rx(bool tx_ena)
{
    if(tx_ena)
        Clear_Reg_Bit(I2C_SLAVE_ADDR,0);
    else
        Write_reg(I2C_SLAVE_ADDR,1);
}

int i2c_port_config(int port_num){
if(port_num >=5)
{
return FAILURE_INVALID_PARAM;
}
//Enable clock-gated i2c..
Write_reg(RCGCI2C_PORT,(1<<1));
Write_reg(RCGCGPIO_CLK,(1<<0));/*Enable Group A GPIO*/
Write_reg(0x40004420,(1<<6)|(1<<7));
Write_reg(0x4000450C,(1<<7));/*Open Drain Configuration for SDA(i2c)*/
Write_reg(0x4000451C,(1<<6)|(1<<7));/*Digital Enable for SDA and SCL*/
Write_reg(0x4000452C,(3<<24)|(3<<28));/*PinMux Control for SDA,SCL*/
Write_reg(I2C_MCS_REG,(1<<4));
Write_reg(I2C_CONFIG,(1<<4));
Write_reg(I2C___MTPR,(0x1));/*Clock configuration for I2C*/

Set_slave_addr(0x57,0);/*Setting Slave Id=0x20 Transmit Data mode*/
return 0;
}

