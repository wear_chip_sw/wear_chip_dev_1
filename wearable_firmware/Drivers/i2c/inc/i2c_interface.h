/*
 * i2c_interface.h
 *
 *  Created on: 11-Aug-2018
 *      Author: MANMATH
 */

#ifndef DRIVERS_I2C_INC_I2C_INTERFACE_H_
#define DRIVERS_I2C_INC_I2C_INTERFACE_H_
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bsp.h"

/*Port And GPIO Config */
result lo_result;
//I2C_PORT portnum;
int i2c_port_config(int port_num);
void Set_slave_addr(uint32_t addr,uint32_t bit_rw);
void Data_tx_rx(bool tx_ena);
void Write_Data(uint8_t data,uint8_t condition);
int read_Data_i2c(uint8_t Addr,uint8_t condition);

typedef enum i2c_condition
{
START=1,
STOP
};

#endif /* DRIVERS_I2C_INC_I2C_INTERFACE_H_ */
