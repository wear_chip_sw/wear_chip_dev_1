/*
 * gpio_driver.h
 *
 *  Created on: 28-Jul-2018
 *      Author: MANMATH
 */
#include "bsp.h"
#include <stdint.h>
#include "TM4C123GH6PM.h"        /* the device specific header (TI) */
#ifndef DRIVERS_GPIO_INC_GPIO_DRIVER_H_
#define DRIVERS_GPIO_INC_GPIO_DRIVER_H_


typedef struct {
    //uint32_t gpio_data;
    uint32_t gpio_dir;
    uint32_t gpio_intrpt_sense;
    uint32_t gpio_intrpt_both_edge;
    uint32_t gpio_intrpt_evnt;
    uint32_t gpio_intrpt_mask;
    uint32_t gpio_intrpt_status;
    uint32_t gpio_intrpt_mask_status;
    uint32_t gpio_intrpt_clear;
    uint32_t gpio_alternet_function_select;
    uint32_t un_used_memory[55];
    uint32_t gpio_drive_strength[3];
    uint32_t gpio_open_drian_sel;
    uint32_t gpio_pull_up_down[2];
    uint32_t gpio_slew_rate_sel;
    uint32_t gpio_digital_enable;
    uint32_t gpio_lock;
    uint32_t gpio_commit;
    uint32_t gpio_analog_mode_select;
    uint32_t gpio_port_control;
    uint32_t gpio_adc_control;
    uint32_t gpio_dma_control;
}gpio_config;


/*Do not Alter the enum sequences It has the Map with ARM_ADDRESS accordingly....*/
typedef enum{
    GPIO_PORT_PA,
    GPIO_PORT_PB,
    GPIO_PORT_PC,
    GPIO_PORT_PD,
    GPIO_PORT_PE,
    GPIO_PORT_PF,
    GPIO_PORT_ERR,
}gpio_port_name;

typedef enum{
    GPIO_ALT_FUN1=0,
    GPIO_ALT_FUN2,
    GPIO_ALT_FUN3,
    GPIO_ALT_FUN4,
    GPIO_ALT_FUN5,
    GPIO_ALT_FUN6,
    GPIO_ALT_FUN7,
    GPIO_ALT_FUN8,
    GPIO_ALT_FUN9,
    GPIO_ALT_FUN10,
    GPIO_ALT_FUN11,
    GPIO_ALT_FUN12,
    GPIO_ALT_FUN13,
    GPIO_ALT_FUN14,
    GPIO_ALT_FUN15,

}gpio_alt_func;

typedef enum{
  GPIO_P_0,
  GPIO_P_1,
  GPIO_P_2,
  GPIO_P_3,
  GPIO_P_4,
  GPIO_P_5,
  GPIO_P_6,
  GPIO_P_7,
  GPIO_P_NA,
}Gpio_numer;

typedef enum{
    GPIO_PIN_MODE_1=1,
    GPIO_PIN_MODE_2,
    GPIO_PIN_MODE_3,
    GPIO_PIN_MODE_4,
    GPIO_PIN_MODE_5,
    GPIO_PIN_MODE_6,
    GPIO_PIN_MODE_7,
    GPIO_PIN_MODE_8,
}gpio_pin_mux_mode;

typedef enum{
    GPIO_PIN_BIT_0=0,
    GPIO_PIN_BIT_4 =4,
    GPIO_PIN_BIT_8=8,
    GPIO_PIN_BIT_12=12,
    GPIO_PIN_BIT_16=16,
    GPIO_PIN_BIT_20=20,
    GPIO_PIN_BIT_24=24,
    GPIO_PIN_BIT_28=28,
}gpio_pin_mux_bits;

typedef struct
{
    uint32_t pin_mux_mode;
    uint32_t pin_mux_bit;
}gpio_pin_mux_cntrl;





typedef struct {
    uint32_t pin_num;
    gpio_port_name port_grp;
    gpio_alt_func gpio_alt;
    gpio_pin_mux_cntrl pin_mux_cntrl;
    uint32_t gpio_dig_ena;
}pin_config;



result lo_result;
Gpio_numer gpio_no;
int gpio_configure_strct(pin_config con_figure ,bool alt_func);
#endif /* DRIVERS_GPIO_INC_GPIO_DRIVER_H_ */
