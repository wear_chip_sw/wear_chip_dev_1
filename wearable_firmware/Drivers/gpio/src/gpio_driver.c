/*
 * gpio_driver.c
 *
 *  Created on: 28-Jul-2018
 *      Author: MANMATH
 */
#include "gpio_driver.h"
#include <stdio.h>

int gpio_configure_strct(pin_config con_figure ,bool alt_func)
{
    if(con_figure.pin_num >= GPIO_P_NA)

        {return FAILURE_INVALID_PARAM;}//Invalid GPIO Group Entered.....

    if(alt_func)
    {
        if(con_figure.gpio_alt == 0)
        {return FAILURE_INVALID_PARAM;}
    }
    #if 0

    config_pb->gpio_alternet_function_select |= 0x1;
    #endif

    switch(con_figure.port_grp)
    {
    case GPIO_PORT_PA:
        config_pa->gpio_alternet_function_select |=(con_figure.gpio_alt)<<(con_figure.pin_num);
        config_pa->gpio_port_control |= (con_figure.pin_mux_cntrl.pin_mux_mode)<<(con_figure.pin_mux_cntrl.pin_mux_bit);
        config_pa->gpio_digital_enable |= 1 <<(con_figure.pin_num);
        break;
    case GPIO_PORT_PB:
        config_pb->gpio_alternet_function_select |=(con_figure.gpio_alt)<<(con_figure.pin_num);
        config_pb->gpio_port_control |= (con_figure.pin_mux_cntrl.pin_mux_mode)<<(con_figure.pin_mux_cntrl.pin_mux_bit);
        config_pb->gpio_digital_enable |= 1 <<(con_figure.pin_num);

        break;
    case GPIO_PORT_PC:
        config_pc->gpio_alternet_function_select |=(con_figure.gpio_alt)<<(con_figure.pin_num);
        config_pc->gpio_port_control |= (con_figure.pin_mux_cntrl.pin_mux_mode)<<(con_figure.pin_mux_cntrl.pin_mux_bit);
        config_pc->gpio_digital_enable |= 1 <<(con_figure.pin_num);
        break;
    case GPIO_PORT_PD:
        config_pd->gpio_alternet_function_select |=(con_figure.gpio_alt)<<(con_figure.pin_num);
        config_pd->gpio_port_control |= (con_figure.pin_mux_cntrl.pin_mux_mode)<<(con_figure.pin_mux_cntrl.pin_mux_bit);
        config_pd->gpio_digital_enable |= 1 <<(con_figure.pin_num);
        break;
    case GPIO_PORT_PE:
        config_pe->gpio_alternet_function_select |=(con_figure.gpio_alt)<<(con_figure.pin_num);
        config_pe->gpio_port_control |= (con_figure.pin_mux_cntrl.pin_mux_mode)<<(con_figure.pin_mux_cntrl.pin_mux_bit);
        config_pe->gpio_digital_enable |= 1 <<(con_figure.pin_num);
        break;
    case GPIO_PORT_PF:
        config_pf->gpio_alternet_function_select |=(con_figure.gpio_alt)<<(con_figure.pin_num);
        config_pf->gpio_port_control |= (con_figure.pin_mux_cntrl.pin_mux_mode)<<(con_figure.pin_mux_cntrl.pin_mux_bit);
        config_pf->gpio_digital_enable |= 1 <<(con_figure.pin_num);
        break;
    default:
        return FAILURE_INVALID_PARAM;

    }
    return SUCCESS;

}



