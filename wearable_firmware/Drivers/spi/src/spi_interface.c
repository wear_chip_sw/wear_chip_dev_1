/*
 * spi_interface.c
 *
 *  Created on: 04-Jun-2018
 *      Author: MANMATH
 */
#include "spi_interface.h"
#include "gpio_driver.h"
#include "bsp.h"
//FIXME:Function to configure the port in more generic way.



uint32_t data_count_test=0;
int spi_port_config(int port_num){
  result res;
    /*Requested Port Num must be less than */
    if(port_num >= 0xF){
        return FAILURE_INVALID_PARAM;
    }

    Write_reg(RCGCSSI_PORT,(1<<2));
    Write_reg(RCGCGPIO_CLK,(1<<1));
    Write_reg(GPIO_AFSL_SPI,((1<<4)|(1<<5)|(1<<6)|(1<<7)));

    Write_reg(GPIO_CTL_SPI,((2<<16)|(2<<20)|(2<<24)|(2<<28)));

    Write_reg(GPIO_DIG_ENB,((1<<4)|(1<<5)|(1<<6)|(1<<7)));
    Write_reg(SSI2_SSCR1,(0<<1));//Disabling SSI Before Use
    Write_reg(SSI2_SSCR1,(0<<2));
    Write_reg(SSI2_SSICC,0x00);
    Write_reg(SSI2_SSICPSR,10);
    Write_reg(SSI2_SSICR0,0x7);
    Write_reg(SSI2_SSIDMACTL,1<<1);/*DMA channel Enable for both Tx and Rx.*/
    Write_reg(SSI2_SSIDMACTL,1);
    Write_reg(SSI2_SSCR1,(1<<1));//Enabling SSI For Use

    return SUCCESS;
}

int spi_port_config_struct(spi_gpio_configure configure_spi)
{
    if(configure_spi.port_num >= 0xF){
        return FAILURE_INVALID_PARAM;
    }
    Write_reg(RCGCSSI_PORT,(1<<2));
    Write_reg(RCGCGPIO_CLK,(1<<1));
        pin_config configure_spi_gpio;

        //Configuration For SPI Master (Clk)
        configure_spi_gpio.pin_num = configure_spi.spio_clk;
        configure_spi_gpio.port_grp= GPIO_PORT_PB;
        configure_spi_gpio.gpio_alt= GPIO_ALT_FUN2;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_bit=GPIO_PIN_BIT_16;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_mode=GPIO_PIN_MODE_2;
        configure_spi_gpio.gpio_dig_ena =configure_spi.spio_clk;
        gpio_configure_strct((pin_config )configure_spi_gpio,true);

        //Configuration For SPI Master (Slave Select)
        configure_spi_gpio.pin_num = configure_spi.spio_ss;
        configure_spi_gpio.port_grp= GPIO_PORT_PB;
        configure_spi_gpio.gpio_alt= GPIO_ALT_FUN2;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_bit=GPIO_PIN_BIT_20;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_mode=GPIO_PIN_MODE_2;
        configure_spi_gpio.gpio_dig_ena =configure_spi.spio_ss;
        gpio_configure_strct((pin_config )configure_spi_gpio,true);

       // Configuration For SPI Master (MISO)
        configure_spi_gpio.pin_num = configure_spi.spio_rx;
        configure_spi_gpio.port_grp= GPIO_PORT_PB;
        configure_spi_gpio.gpio_alt= GPIO_ALT_FUN2;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_bit=GPIO_PIN_BIT_24;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_mode=GPIO_PIN_MODE_2;
        configure_spi_gpio.gpio_dig_ena =configure_spi.spio_rx;
        gpio_configure_strct((pin_config )configure_spi_gpio,true);

        //Configuration For SPI Master (MOSI)
        configure_spi_gpio.pin_num = configure_spi.spio_tx;
        configure_spi_gpio.port_grp= GPIO_PORT_PB;
        configure_spi_gpio.gpio_alt= GPIO_ALT_FUN2;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_bit=GPIO_PIN_BIT_28;
        configure_spi_gpio.pin_mux_cntrl.pin_mux_mode=GPIO_PIN_MODE_2;
        configure_spi_gpio.gpio_dig_ena =configure_spi.spio_tx;
        gpio_configure_strct((pin_config )configure_spi_gpio,true);



    Write_reg(SSI2_SSCR1,(0<<1));//Disabling SSI Before Use
    Write_reg(SSI2_SSCR1,(0<<2));
    Write_reg(SSI2_SSICC,0x00);
    Write_reg(SSI2_SSICPSR,10);
    Write_reg(SSI2_SSICR0,0x7);
    //Write_reg(SSI2_SSIDMACTL,1<<1);/*DMA channel Enable for both Tx and Rx.*/
    Write_reg(SSI2_SSIDMACTL,1);
    Write_reg(SSI2_SSCR1,(1<<1));//Enabling SSI For Use
    Write_reg(0x4000A014,1<<3);
    /*Enabling Interrupts */
    //NVIC_EnableIRQ(SSI2_IRQn);
    //Write_reg(0xE000E104,1<<57);
    return SUCCESS;

}

void Async_write_spi(uint8_t data)
{
    Write_reg(0x4000A014,1<<3);
    Write_reg(SSI2_SSIDR,data);
    while(((*((unsigned int *)SSI2_SSISR))&(1<<0)) != 0);
}

void Async_write_spi_dma()
{

    UDMA_CNTL_END_PTR test; //FIXME:Remove It After Testing The Function
    test.Transfer_mode= 1;
    test.Next_time_brust=0;
    test.XferSize=16;
    test.arbitation_size=2;
    test.reserved=0;
    test.source_data_size=0;
    test.source_addr_inc=0;
    test.destination_data_size=0;
    test.destination_addr_inc=0x03;
    udma_config configure;
    configure.Perh_type=PERIPHERAL_TYPE_2; //FIXME:Remove It After Testing The Function
    configure.chan_num =DMA_CH13;
    configure.Source_Data_Buffer=src_data_buff;
    configure.Src_Buff_Size=256;
    configure.DMA_CHA_CNTRL_STRCT =(UDMA_CNTL_END_PTR)test;
    configure.Destination_Data_Buffer=&(SSI2->DR);
    Udma_ConfigureChannelControlStructure(configure);
    Start_DMA_TX(configure.chan_num,configure.Perh_type);
}

int Async_read_spi()
{
    int data;
    while(((*((unsigned int *)SSI2_SSISR))&(1<<0)) == 0)
    {
        data=(*((unsigned int *)SSI2_SSIDR));
    }
    return data;
}


void SSI2_IRQHandler()
{

//FIXME:Write proper ISR as per the requirement
    data_count_test++;
    Write_reg(0x4000A020,0x3);
    Write_reg(0x4000A004,1<<4);
//    Async_write_spi(0x1);


}
