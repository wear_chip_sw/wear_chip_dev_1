/*
 * spi_interface.h
 *
 *  Created on: 04-Jun-2018
 *      Author: MANMATH
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bsp.h"
#include "udma_interface.h"

/*
 * PB4 58 AIN10 - SSI2Clk
 * PB5 57 AIN11 - SSI2Fss
 * PB6 1 - - SSI2Rx
 * PB7 4 - - SSI2Tx
 */

result lo_result;
bool init;
typedef struct {
    uint8_t port_num ;
    uint32_t spio_clk;
    uint32_t spio_ss;
    uint32_t spio_rx;
    uint32_t spio_tx;
}spi_gpio_configure;
int spi_port_config(int port_num);
int spi_port_config_struct(spi_gpio_configure configure_spi);
void Async_write_spi(uint8_t data);
void Async_write_spi_dma();
int Async_read_spi();

