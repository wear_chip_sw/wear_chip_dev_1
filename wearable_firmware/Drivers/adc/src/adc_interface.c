/*
 * adc_interface.c
 *
 *  Created on: 15-Sep-2018
 *      Author: MANMATH
 */
#include "adc_interface.h"
#include "TM4C123GH6PM.h"
#include "qpc.h"
#include "bsp.h"
Q_DEFINE_THIS_FILE

int adc_init(uint32_t adc_num)
{
// if(adc_num >= ADC_PORT_NA)         /*We have 2 ADC port i.e ADC0 and ADC1*/
	// Q_onAssert(1);
 Q_REQUIRE(adc_num <= ADC_PORT_NA);

 /*Enable the ADC clock using the RCGCADC register (see page 352).*/
 //RCGCADC__PORT
 SYSCTL->RCGCADC |= (1<<adc_num);
 //SWrite_reg(RCGCADC__PORT,(1<<adc_num));
 Write_reg(RCGCGPIO_CLK,(1<<4));/*Enable Group E GPIO*/
 //GPIOADC__DIR
 Clear_Reg_Bit(GPIOADC__DIR,(1<<1));/*PE1 Will act as an Analog Input to the Pin*/
 Write_reg(0x40024420,(1<<1));/*Selecting Alternative function  for PE1*/
 Clear_Reg_Bit(0x4002451C,(1<<1));/*Since we are Using Analog I/p So Digital Enable should be cleared*/
 Write_reg(0x40024528,(1<<1)); /*Disabling Analog isolation Circuits by setting corresponding Bits here PE1*/

return SUCCESS;
}

int config_sample_seq()
{

Clear_Reg_Bit(ADC_1__ACTSS,(1<<3)); /*STEP1:-Disable Sample Sequenter Before Use*/
Write_reg(ADC_1___EMUX,(0x4<<12));/*Select Sample Sequenter3 to write 0xf for Continious sampling or 0x4 for gpio int*/
Write_reg(ADC_1_SSMUX3,2); /*Sample Sequenter3 Mux AIN2 config*/
Write_reg(ADC_1_SSCTL3,0x6); /*Sample Sequenter3 control register configuration */
Write_reg(ADC_1_INTMSK,(1<<3)); /*Un-masking Interupt to use It*/
Write_reg(ADC_1__ACTSS,(1<<3)); /*Enable Sample Sequenter Before Use*/
Write_reg(ADC_1__ICS,(1<<3));/*Clearing Interupt status*/
//NVIC_EnableIRQ(ADC1SS3_IRQn);
return SUCCESS;
}





