/*
 * adc_interface.h
 *
 *  Created on: 15-Sep-2018
 *      Author: MANMATH
 */

#ifndef DRIVERS_ADC_INC_ADC_INTERFACE_H_
#define DRIVERS_ADC_INC_ADC_INTERFACE_H_
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bsp.h"

result io_result;
typedef enum
{
ADC_PORT0,
ADC_PORT1,
ADC_PORT_NA
}ADC_PORT;

volatile static uint32_t adc_data_out = 0;
int adc_init(uint32_t adc_num);

int config_sample_seq();



#endif /* DRIVERS_ADC_INC_ADC_INTERFACE_H_ */
