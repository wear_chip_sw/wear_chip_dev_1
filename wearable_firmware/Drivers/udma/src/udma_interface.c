/*
 * udma_interface.c
 *
 *  Created on: 14-Jun-2018
 *      Author: MANMATH
 */
#include "udma_interface.h"
#include <stdbool.h>
#include <stdio.h>
uint32_t __attribute__((section(".dma_cntl_ch"))) uDMAChannelControl[256];

void fill_data()
{
    for (int i=0;i<DATA_BUFFER_SIZE;i++)
    {
      src_data_buff[i]=0x61;
    }

}


int Start_DMA_TX(uint32_t Channel_num,perh_sel perph_type)
{
  dma_channel_num ch_num =DMA_CH31;
    if(Channel_num >= ch_num )
    {
        //Unsupported Channel NUmber
        return -1;
    }

/*Check If Channel is Configured For Software Use I.e Only Copying Within memory*/
    if(perph_type == PERIPHERAL_TYPE_4)
    {
        UDMA->ENASET = 1<< Channel_num;
        UDMA->SWREQ = 1<< Channel_num;

    }
    else
    {
        UDMA->ENASET = 1<< Channel_num;

    }
return 0;
}



int Udma_ConfigureChannelControlStructure(udma_config configure)
{
    result lo_res=SUCCESS;

    /*Pre-Requsite:Channel Attributes
     * 1.Setting Channle Prioroty*/
    UDMA->PRIOSET = 1<<configure.chan_num;
    UDMA->ALTCLR  = 1<<configure.chan_num;
    UDMA->USEBURSTCLR = 1<<configure.chan_num;
    UDMA->REQMASKCLR  = 1<<configure.chan_num;




/*STEP 1:Choose Proper Peripherals as per MUXED Value*/
    UDMA->CHMAP1 = (configure.Perh_type)<<20;
    //Write_reg(DMA_CHANEL_MAP_01,configure.Perh_type);

/*STEP 2:Configure Following Pointers
 * 1.Control Source pointer i.e Address of uDMAChannelControl+(Channel Num*0x10+DMASRCENDP_OFFSET)
 * 2.Control Destination pointer i.e Address of uDMAChannelControl+(Channel Num*0x10+DMADSTENDP_OFFSET)
 * 3.Control Channel Pointer i.e Address of uDMAChannelControl+(Channel Num*0x10+DMACHCTL_OFFSET)
 *
 * */
    uint32_t ctrl_src_ptr = (((unsigned int )uDMAChannelControl)+((configure.chan_num*0x10)+DMASRCENDP_OFFSET));
    uint32_t ctrl_dst_ptr = (((unsigned int )uDMAChannelControl)+((configure.chan_num*0x10)+DMADSTENDP_OFFSET));
    uint32_t ctrl_end_ptr = (((unsigned int )uDMAChannelControl)+((configure.chan_num*0x10)+DMACHCTL_OFFSET));

/*STEP 3:
 * 1.Write The Source Data End pointer to Control Source pointer
 * 2.Write The Destination Data End pointer to Control Destination pointer
 * 3.Write The Control Channel End pointer to Control Control Channel Pointer
 * */
    if(configure.DMA_CHA_CNTRL_STRCT.source_addr_inc !=0x03)
        {
        Write_reg(ctrl_src_ptr,(((unsigned int )configure.Source_Data_Buffer)+configure.Src_Buff_Size));
        }
    else{
        Write_reg(ctrl_src_ptr,((unsigned int )configure.Source_Data_Buffer));
        }

    if(configure.DMA_CHA_CNTRL_STRCT.destination_addr_inc !=0x03)
        {
            Write_reg(ctrl_dst_ptr,(((unsigned int )configure.Destination_Data_Buffer)+configure.Dstn_Buff_Size));
        }
        else{
            Write_reg(ctrl_dst_ptr,((unsigned int )configure.Destination_Data_Buffer));
        }
    uint32_t Channel_config_Param=Read_reg(&(configure.DMA_CHA_CNTRL_STRCT));
    Write_reg(ctrl_end_ptr,Channel_config_Param);

    /*STEP 4:Enabling Channel For Tx/Rx*/
   // UDMA->ENASET=1<<configure.chan_num;
    Write_reg(SSI2_SSIDMACTL,1<<1);//DMA channel Enable for both Tx and Rx.
   // Write_reg(SSI2_SSIDMACTL,1);

    return lo_res;
}

int init_udma()
{
uint32_t udma_ctl = ((unsigned int )uDMAChannelControl);
/*NVIC_EnableIRQ(46);
NVIC_EnableIRQ(47);*/
fill_data();//FIXME:Temporary Function to Fill the Source buffer,Need to remove from Original Code.
SYSCTL->RCGCDMA = UDMA_CLK__GATE_ENABLE;
UDMA->CFG = UDMA_MASTER_ENB;
UDMA->CTLBASE = 0x0;    //Erasing all Data From COntrol-base before writing the uDMAChannelControl
UDMA->CTLBASE = udma_ctl;

//Setting Up channel Attributes for Periph-Memory DMA Oppeartions

return 0;
}

void uDMAST_IRQHandler(void)
{
 //FIXME:Proper ISR Needs to be Written As per the Use.
uint32_t i=Read_reg(0x400FF504);
if(i)
{}

}

void uDMAError_IRQHandler(void)
{
    //FIXME:Proper ISR Needs to be Written As per the Use.
    uint32_t i=Read_reg(0x400FF504);
    if(i)
    {}

}

