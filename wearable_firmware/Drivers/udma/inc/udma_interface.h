/*
 * udma_interface.h
 *
 *  Created on: 14-Jun-2018
 *      Author: MANMATH
 */

#ifndef DRIVERS_UDMA_INC_UDMA_INTERFACE_H_
#define DRIVERS_UDMA_INC_UDMA_INTERFACE_H_
#include "TM4C123GH6PM.h"
#include "bsp.h"
#include <stdio.h>
#include <stdbool.h>
#pragma DATA_ALIGN(uDMAChannelControl, 1024)

uint32_t src_data_buff[256];
uint32_t dst_data_buff[256];

#define DATA_BUFFER_SIZE  256
#define DMASRCENDP_OFFSET 0x000  // DMA Channel Source Address End Pointer
#define DMADSTENDP_OFFSET 0x004  // DMA Channel Destination Address End Pointer
#define DMACHCTL_OFFSET 0x008    // DMA Channel Control Word


typedef struct __attribute__((__packed__)) Udma_Cntrl_Strct
{

  uint32_t Transfer_mode         :3;    /*0x00--STOP;0x01--BASIC;0X02--AUTO-REQ;0X03--PING-PONG;0X04--(MEMORY)SCATTER-GATHER;0X05--(ALT-MEMORY)SCATTER-GATHER;
                                          0X06--(PERIPHER)SCATTER-GATHER;0X07--(ALT-PERIPHER)SCATTER-GATHER*/
  uint32_t Next_time_brust       :1;
  uint32_t XferSize             :10;    /*Data Will vary from 1 to 1023.THis will be the tottal number of data bytes needs to Transfer*/
  uint32_t arbitation_size       :4;     /*value should be between 0x0--0xA*/
  uint32_t reserved              :6;     /*Reserved */
  uint32_t source_data_size      :2;     /*0x00--1 Byte,0x01--2 Bytes(Half Word),0x2--4 Bytes(Word),0x03--Reserved*/
  uint32_t source_addr_inc       :2;     /*0x00--1 Byte,0x01--2 Bytes(Half Word),0x2--4 Bytes(Word),0x03--No Increment*/
  uint32_t destination_data_size :2;     /*0x00--1 Byte,0x01--2 Bytes(Half Word),0x2--4 Bytes(Word),0x03--Reserved*/
  uint32_t destination_addr_inc  :2;     /*0x00--1 Byte,0x01--2 Bytes(Half Word),0x2--4 Bytes(Word),0x03--No Increment*/
}UDMA_CNTL_END_PTR;
/*Clock Enable Or Disable */
int init_udma();

//DMA Offers 32 Channels.Each channel is muxed with 4 type of Use.
typedef enum Dma_Channels
{
    DMA_CH0,
    DMA_CH1,
    DMA_CH2,
    DMA_CH3,
    DMA_CH4,
    DMA_CH5,
    DMA_CH6,
    DMA_CH7,
    DMA_CH8,
    DMA_CH9,
    DMA_CH10,
    DMA_CH11,
    DMA_CH12,
    DMA_CH13,
    DMA_CH14,
    DMA_CH15,
    DMA_CH16,
    DMA_CH17,
    DMA_CH18,
    DMA_CH19,
    DMA_CH20,
    DMA_CH21,
    DMA_CH22,
    DMA_CH23,
    DMA_CH24,
    DMA_CH25,
    DMA_CH26,
    DMA_CH27,
    DMA_CH28,
    DMA_CH29,
    DMA_CH30,
    DMA_CH31

}dma_channel_num;

typedef enum Peripheral_type
{
    PERIPHERAL_TYPE_0,
    PERIPHERAL_TYPE_1,
    PERIPHERAL_TYPE_2,
    PERIPHERAL_TYPE_3,
    PERIPHERAL_TYPE_4, //Type 4 is basically used for Software Use Only

}perh_sel;

int Start_DMA_TX(uint32_t Channel_num, perh_sel perph_type);


uint16_t primaryDataBuffer[DATA_BUFFER_SIZE];
uint16_t alternateDataBuffer[DATA_BUFFER_SIZE];

typedef enum dma_chan_type{
    PRIMERY,
    ALTERNATE,
    CHAN_TYPE_NA
}dma_chan_type ;


/*Dummy function for Filling Random data For Spi Transfer*/
void fill_data();
/*
 * Structure Udma_Config__ Basically used to Initialize all required Params to Perform UDMA Opperations
 * Source_Data_Buffer---Source Data Buffer Pointer Where Data is located
 * Src_Buff_Size----Size of Buffter Data need to trasmit
 * Destination_Data_Buffer--IF Software Transmission is perferming I.e For Memory-to-Memory this can be any Address.
 *                          But For Peripheral Transmission This should be Data Register Address.
 * Dstn_Buff_Size-----Only Applicable for Mem-to-Mem Transfer
 * Peripheral_type------Chose the proper Peripheral from Muxed Data
 * Dma_Channels----------Selecet the Channel Number
 * dma_chan_type---------Applicable Only for Ping-Pong Mode of Transmission
 * Udma_Cntrl_Strct------Struct to configure Control-Struct for Configured Channel
 * */

typedef struct Udma_Config__
{
    uint32_t *Source_Data_Buffer;
    uint32_t Src_Buff_Size;
    uint32_t *Destination_Data_Buffer;
    uint32_t Dstn_Buff_Size;
    perh_sel Perh_type;
    dma_channel_num chan_num;
    dma_chan_type Chan_type;
    UDMA_CNTL_END_PTR DMA_CHA_CNTRL_STRCT;

}udma_config;

/*
 * Function To Configure the Following Parameters Given Below
 * 1.Channel Number to Perform proper Muxed Peripherla Use
 * 2.Src and Destination Data End pointer
 * 3.Control struct for Data transfer
 *
 * */
int Udma_ConfigureChannelControlStructure(udma_config configure);

#endif /* DRIVERS_UDMA_INC_UDMA_INTERFACE_H_ */
