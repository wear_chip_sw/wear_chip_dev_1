##############################################################################
# Product: Makefile for QP/C port to Win32, MinGW toolset
# Last updated for version 6.3.6
# Last updated on  2018-10-13
#
#                    Q u a n t u m  L e a P s
#                    ------------------------
#                    Modern Embedded Software
#
# Copyright (C) 2002-2018 Quantum Leaps, LLC. All rights reserved.
#
# This program is open source software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alternatively, this program may be distributed and modified under the
# terms of Quantum Leaps commercial licenses, which expressly supersede
# the GNU General Public License and are specifically designed for
# licensees interested in retaining the proprietary status of their code.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Contact information:
# https://www.state-machine.com
# mailto:info@state-machine.com
##############################################################################
# examples of invoking this Makefile:
# building configurations: Debug (default), Release and Spy
# make
# make CONF=rel
# make CONF=spy
#
# cleaning configurations: Debug (default), Release, and Spy
# make clean
# make CONF=rel clean
# make CONF=spy clean
#
# NOTE:
# To use this Makefile on Windows, you will need the GNU make utility, which
# is included in the Qtools collection for Windows, see:
#    http://sourceforge.net/projects/qpc/files/Qtools/
#

#-----------------------------------------------------------------------------
# project name
#
PROJECT     := WEARABLE_0_0_1

#-----------------------------------------------------------------------------
# project directories
#

# location of the QP/C framework
QPC := ./qpc
BIN_DIR := build
###########################################################
#
#Configure The Build Varity  Option
#
###########################################################


###########################################################
#
#Configure The Tool-Chain Option
#
###########################################################
ifeq (gnu, $(TOOLCHAIN))
TOOL	:= gnu
ifeq (rel, $(CONF))
VARIETY := rel
CPY_BSP := $(QPC)\..\wearable_firmware\bsp_0.1\BSP_BIN_GNU_REL
else ifeq (spy, $(CONF))
VARIETY := spy
CPY_BSP := $(QPC)\..\wearable_firmware\bsp_0.1\BSP_BIN_GNU_SPY
else
VARIETY := dbg
CPY_BSP := $(QPC)\..\wearable_firmware\bsp_0.1\BSP_BIN_GNU_DBG
endif
CPY_APP	:= $(QPC)\..\wearable_firmware\app\wearable-$(TOOL)-$(VARIETY)
else ifeq (llvm, $(TOOLCHAIN))
TOOL	:= llvm
else
$(error Tool-Chain Configuartion Not Entered)
endif

MKDIR := mkdir
RM	  := rm
RF 	  := rf



# create $(BIN_DIR) if it does not exist
ifeq ("$(wildcard $(BIN_DIR))","")
$(shell $(MKDIR) $(BIN_DIR))
endif

subsystem: 
	$(MAKE) CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/ports/arm-cm/qxk
	$(MAKE) CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/../wearable_firmware/bsp_0.1
	$(MAKE) CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/../wearable_firmware/Drivers
	$(MAKE) CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/../wearable_firmware/app
	$(MAKE) copy
 	
copy:
	cp -R $(CPY_BSP) $(BIN_DIR)
	cp -R $(CPY_APP) $(BIN_DIR)
.PHONY : clean
clean:
	$(shell $(RM) -$(RF) $(BIN_DIR))
	$(MAKE) clean CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/ports/arm-cm/qxk
	$(MAKE) clean CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/../wearable_firmware/bsp_0.1
	$(MAKE) clean CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/../wearable_firmware/app
	$(MAKE) clean CONF=$(VARIETY) TOOLCHAIN=$(TOOL) -C $(QPC)/../wearable_firmware/Drivers
	